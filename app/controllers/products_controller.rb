class ProductsController < ApplicationController
    before_action :logged_in, except: [:index]

    def create
        @product = Product.create(strong_params)
        if @product.save
            flash.now[:success] = 'A product has been created'
            render 'users/show'
        else
            flash.now[:danger] = 'All fields/images need to be filled'
            render 'users/show'
        end
    end

    def index
        @products = Product.all.paginate(page: params[:page], per_page: 5)
    end

    def destroy
        @product = Product.find_by(id: params[:id])
        @product.delete
        flash[:success] = 'A product has been deleted'
        redirect_to products_path
    end

    private

    def strong_params
        params.require(:product).permit(:name, :description, :sku, :picture)
    end
end
