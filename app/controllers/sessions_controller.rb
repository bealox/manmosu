class SessionsController < ApplicationController
    def create
        @user = User.find_by(email: params[:session][:email])
        if @user && @user.authenticate(params[:session][:password])
            log_in(@user)
            redirect_to user_path(@user)
        else
            flash.now[:danger] = 'Invalid details.'
            render 'new'
        end
    end

    def new; end

    def delete
        log_out
        redirect_to root_path
    end
end
