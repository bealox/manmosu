class StaticPagesController < ApplicationController
    # TODO:
    # organise gemfile
    # setup bootrap and jquery

    before_action :check_contact?, only: [:send_contact]

    def home; end

    def contact; end

    def send_contact
        if check_contact?
            ContactMailer.contact_us(@email, body: @body, name: @name).deliver_now
            flash['success'] = 'Thank you for contacting us, we will contact you shortly.'
            redirect_to contact_path
        else
            flash.now[:danger] = 'Please fill in all the details.'
            render 'contact'
        end
    end

    private

    def check_contact?
        @email = params[:contact][:email]
        @name = params[:contact][:name]
        @body = params[:contact][:body]

        !@email.nil? && !@name.nil? && !@body.nil?
    end
end
