class ApplicationMailer < ActionMailer::Base
    default from: 'contact@manmosu.com.au'
    layout 'mailer'
end
