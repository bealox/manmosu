class ContactMailer < ApplicationMailer
    # Subject can be set in your I18n file at config/locales/en.yml
    # with the following lookup:
    #
    #   en.contact_mailer.contact_us.subject
    #
    def contact_us(email, body: 'no body ', name: 'no name')
        @body = body
        @name = name
        @email = email
        mail to: 'contact@manmosu.com.au', subject: 'Contact us'
    end
end
