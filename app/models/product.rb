class Product < ApplicationRecord
    validates :name, :description, presence: true
    validate :picture_size

    default_scope -> { order(created_at: :desc) }

    mount_uploader :picture, PictureUploader

    require 'RedCloth'

    def description_html
        RedCloth.new(description).to_html
    end

    private

    def picture_size
        if picture.size > 5.megabytes
            errors.add(:picture, 'should be less than 5MB')
        end
    end
end
