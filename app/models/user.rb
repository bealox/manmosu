class User < ApplicationRecord
    validates :name, :email, presence: true

    has_secure_password

    def self.create_digest(password)
        cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
        BCrypt::Password.create(password, cost: cost)
    end
end
