Rails.application.routes.draw do
    post '/login', to: 'sessions#create'
    delete '/logout', to: 'sessions#delete'
    get '/login', to: 'sessions#new'
    root 'static_pages#home'
    get '/contact', to: 'static_pages#contact'
    post '/contact', to: 'static_pages#send_contact'

    resources :products, except: [:edit, :update, :show, :new]
    resources :users, only: [:show]
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
