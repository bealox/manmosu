class AddDescriptionToProducts < ActiveRecord::Migration[5.0]
    def change
        add_column :products, :description, :string
        add_index :products, :created_at
        add_index :users, :email
    end
end
