require 'test_helper'

class ProductsControllerTest < ActionDispatch::IntegrationTest
    def setup
        @user = users(:eddie)
        @yoga_product = products(:yoga_mat)
    end

    test 'test product validation' do
        log_in_as @user

        post products_path params: {
            product: {
                name: 'hello',
                description: ''
            }
        }

        product = assigns(:product)
        assert !product.errors.empty?

        post products_path params: {
            product: {
                name: '',
                description: 'hello'
            }
        }

        product = assigns(:product)
        assert !product.errors.empty?
        picture = fixture_file_upload('test/fixtures/rails.jpg', 'image/jpg')
        post products_path params: {
            product: {
                name: 'hello',
                description: 'hello',
                picture: picture
            }
        }

        product = assigns(:product)
        puts "dog #{product.errors.full_messages}"
        assert product.errors.empty?
    end

    test 'need to log in to create/delete products' do
        # index can access by anyone
        get products_path
        assert_template 'products/index'
        # create new and delete has to be logged in

        assert_no_difference 'Product.count' do
            post products_path params: {
                product: {
                    name: 'hello',
                    description: 'hello'
                }
            }
        end

        assert_no_difference 'Product.count' do
            delete product_path(@yoga_product)
        end

        assert !flash.empty?
        assert_redirected_to login_path

        log_in_as @user
        picture = fixture_file_upload('test/fixtures/rails.jpg', 'image/jpg')
        assert_difference 'Product.count', 1 do
            post products_path params: {
                product: {
                    name: 'hello',
                    description: 'hello',
                    picture: picture
                }
            }
        end

        product = assigns(:product)

        assert_difference 'Product.count', -1 do
            delete product_path(product)
        end
    end

    test 'index with data' do
        get products_path
        assert_template 'products/index'
        assert_select 'div.panel-body', 5
    end
end
