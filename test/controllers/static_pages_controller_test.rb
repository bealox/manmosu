require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest
    def setup
        ActionMailer::Base.deliveries.clear
    end

    test 'should get home' do
    end

    test 'send_contact action' do
        get contact_path
        assert_template 'static_pages/contact'

        post contact_path, params: {
            contact: {
                name: 'test'
            }
        }

        assert !flash.empty?

        post contact_path, params: {
            contact: {
                email: 'test'
            }
        }
        assert !flash.empty?

        post contact_path, params: {
            contact: {
                name: 'Andy',
                email: 'test@test.com',
                body: 'body'
            }
        }

        assert !ActionMailer::Base.deliveries.empty?
    end
end
