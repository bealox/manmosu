require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
    def setup
        @user = users(:eddie)
    end

    test 'logged in required to access show' do
        get user_path(@user)
        assert_redirected_to login_path

        log_in_as @user
        assert logged_in?
        get user_path(@user)
        assert_template 'users/show'
    end

    # interface
    test 'show interface ' do
        log_in_as @user
        follow_redirect!
        assert_template'users/show'
        assert_select 'input[type=file]', 1
        assert_select 'input[type=text]', 2

        name = 'test'
        description = 'description'
        picture = fixture_file_upload('test/fixtures/rails.jpg', 'image/jpg')

        assert_difference 'Product.count', 1 do
            post products_path, product: {
                name: name,
                description: description,
                picture: picture
            }
        end

        product = assigns(:product)

        assert product.picture?

        # assert_select 'textarea', 2
    end
end
