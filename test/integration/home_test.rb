require 'test_helper'

class HomeTest < ActionDispatch::IntegrationTest
    # test "the truth" do
    #   assert true
    # end
    test 'check header design' do
        # should have 2 links and one logo
        get root_path
        assert_template 'static_pages/home'
        assert_select 'a[href=?]', contact_path, 1
        assert_select 'a[href=?]', products_path, 1
    end
end
