require 'test_helper'

class LoginTest < ActionDispatch::IntegrationTest
    # test "the truth" do
    #   assert true
    # end

    # test 'login'
    def setup
        @user = users(:eddie)
    end

    test 'test log in/out function and interface' do
        get login_path
        assert_template 'sessions/new'
        post login_path, params: {
            session: {
                email: '',
                password: 'password'
            }
        }

        assert !flash.empty?

        post login_path, params: {
            session: {
                email: @user.email,
                password: ''
            }
        }

        assert !flash.empty?

        post login_path, params: {
            session: {
                email: @user.email,
                password: 'password'
            }
        }

        assert_redirected_to user_path(@user)
        assert logged_in?

        follow_redirect!
        assert_select 'ul.navbar-right'
        assert_select 'a[href=?]', logout_path, 1

        delete logout_path
        assert_redirected_to root_path

        follow_redirect!
        assert_select 'ul.navbar-right', 0
        assert_select 'a[href=?]', logout_path, 0
    end
end
