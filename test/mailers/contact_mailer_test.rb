require 'test_helper'

class ContactMailerTest < ActionMailer::TestCase
    test 'contact_us' do
        mail = ContactMailer.contact_us('test@test.com', body: 'hello', name: 'Andy')
        assert_equal 'Contact us', mail.subject
        assert_equal ['contact@manmosu.com.au'], mail.to
        assert_equal ['contact@manmosu.com.au'], mail.from
        assert_match 'hello', mail.body.encoded
        assert_match 'Andy', mail.body.encoded
    end
end
