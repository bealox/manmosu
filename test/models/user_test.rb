require 'test_helper'

class UserTest < ActiveSupport::TestCase
    # test "the truth" do
    #   assert true
    # end

    test 'create digest' do
        hash_password = User.create_digest('password')
        assert BCrypt::Password.new(hash_password).is_password?('password')
    end
end
